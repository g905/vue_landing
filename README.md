# electron

> Верстка "Электрон" лендинг вакансий

## Установка

``` bash
# Зависимости
npm install

# Вебпак-дев-сервер с хот-релоадом на localhost:8080
npm run dev

# продакшн билд
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
