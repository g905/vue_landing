import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/js/bootstrap.js'
import Vue from 'vue'
import App from './App.vue'
Vue.use(BootstrapVue)

new Vue({
  el: '#app',
  render: h => h(App)
})
